package br.com.devmedia.orm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	private static final String DB = "jdbc:mysql://localhost:3306/cdm_api_rest1";
	private static final String USER = "root";
	private static final String PASSWORD = "";
	
	public static Connection getConnection()
		throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(DB, USER, PASSWORD);
	}
	
}