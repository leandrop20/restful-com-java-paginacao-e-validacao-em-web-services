package br.com.devmedia.orm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.Statement;

import br.com.devmedia.domain.Lembrete;
import br.com.devmedia.exception.ApiException;

public class LembreteMapper {

	public Lembrete select(Lembrete lembrete) throws ApiException {
		try {
			Connection conn = Database.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"SELECT * FROM lembretes WHERE id = ?"
			);
			statement.setInt(1, lembrete.getId());
			ResultSet rs = statement.executeQuery();
			
			if (rs.next()) {
				lembrete = new Lembrete(
					rs.getInt("id"),
					rs.getString("titulo"),
					rs.getString("descricao")
				);
			} else {
				lembrete = null;
			}
		} catch (Exception e) {
			throw new ApiException(500, e.getMessage());
		}
		return lembrete;
	}
	
	public Lembrete insert(Lembrete lembrete) throws ApiException {
		try {
			Connection conn = Database.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"INSERT INTO lembretes (titulo, descricao)"
				+ " VALUES (?, ?)",
				Statement.RETURN_GENERATED_KEYS
			);
			statement.setString(1, lembrete.getTitulo());
			statement.setString(2, lembrete.getDescricao());
			statement.execute();
			
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				lembrete.setId(rs.getInt(1));
			}
		} catch (Exception e) {
			throw new ApiException(500, e.getMessage());
		}
		return this.select(lembrete);
	}
	
	public Lembrete update(Lembrete lembrete) throws ApiException {
		try {
			Connection conn = Database.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"UPDATE lembretes SET titulo = ?, descricao = ?"
				+ " WHERE id = ?"
			);
			statement.setString(1, lembrete.getTitulo());
			statement.setString(2, lembrete.getDescricao());
			statement.setInt(3, lembrete.getId());
			statement.execute();
			
			if (statement.getUpdateCount() == 0) {
				throw new ApiException(
					404,
					"O lembrete informado n�o existe."
				);
			}
		} catch (ApiException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ApiException(
				500,
				"Erro n�o especificado: " + e.getMessage()
			);
		}
		return this.select(lembrete);
	}
	
	public Lembrete delete(Lembrete lembrete) throws ApiException {
		try {
			Connection conn = Database.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"DELETE FROM lembretes WHERE id = ?"
			);
			statement.setInt(1, lembrete.getId());
			statement.execute();
			
			if (statement.getUpdateCount() == 0) {
				throw new ApiException(
					404,
					"O lembrete informado n�o existe."
				);
			}
		} catch (ApiException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ApiException(
				500,
				"Erro n�o especificado: " + e.getMessage()
			);
		}
		return null;
	}
	
}
