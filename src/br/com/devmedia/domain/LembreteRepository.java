package br.com.devmedia.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.devmedia.exception.ApiException;
import br.com.devmedia.orm.Database;

public class LembreteRepository {

	public static final int PAGE_LENGTH = 5;

	public List<Lembrete> getByRange(int page) throws ApiException {
		List<Lembrete> lista = new ArrayList<Lembrete>();
		try {
			Connection conn = Database.getConnection();
			
			page = (page -1) * PAGE_LENGTH;
			
			PreparedStatement statement = conn.prepareStatement(
				"SELECT * FROM lembretes LIMIT ?,?"
			);
			statement.setInt(1, page);
			statement.setInt(2, PAGE_LENGTH);
			
			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				Lembrete lembrete = new Lembrete(
					rs.getInt("id"),
					rs.getString("titulo"),
					rs.getString("descricao")
				);
				lista.add(lembrete);
			}
		} catch (Exception e) {
			throw new ApiException(
				500,
				"Erro ao paginar os dados do BD" + e.getMessage()
			);
		}
		return lista;
	}

	public List<Lembrete> getAll() throws ApiException {
		List<Lembrete> lista = new ArrayList<Lembrete>();
		try {
			Connection conn = Database.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"SELECT * FROM lembretes LIMIT " + PAGE_LENGTH
			);
			
			ResultSet rs = statement.executeQuery();
			
			while(rs.next()) {
				Lembrete lembrete = new Lembrete(
					rs.getInt("id"),
					rs.getString("titulo"),
					rs.getString("descricao")
				);
				lista.add(lembrete);
			}
		} catch (Exception e) {
			throw new ApiException(
				500,
				"Erro ao buscar os dados no BD." + e.getMessage()
			);
		}
		return lista;
	}

}
